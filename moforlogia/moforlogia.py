import cv2
import numpy as np
from matplotlib import pyplot as plt

# Leer la imagen binaria
I = cv2.imread('hola.png', cv2.IMREAD_GRAYSCALE)

# Crear un elemento estructurante
se = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (7, 7))  # 7x7 es equivalente a un disco de radio 3 en cv2

# Dilatación
I_dilated = cv2.dilate(I, se)

# Erosión
I_eroded = cv2.erode(I, se)

# Apertura
I_opened = cv2.morphologyEx(I, cv2.MORPH_OPEN, se)

# Cierre
I_closed = cv2.morphologyEx(I, cv2.MORPH_CLOSE, se)

# Mostrar las imágenes
plt.figure(figsize=(10, 6))

plt.subplot(2, 3, 1)
plt.imshow(I, cmap='gray')
plt.title('Original')
plt.axis('off')

plt.subplot(2, 3, 2)
plt.imshow(I_dilated, cmap='gray')
plt.title('Dilatación')
plt.axis('off')

plt.subplot(2, 3, 3)
plt.imshow(I_eroded, cmap='gray')
plt.title('Erosión')
plt.axis('off')

plt.subplot(2, 3, 4)
plt.imshow(I_opened, cmap='gray')
plt.title('Apertura')
plt.axis('off')

plt.subplot(2, 3, 5)
plt.imshow(I_closed, cmap='gray')
plt.title('Cierre')
plt.axis('off')

plt.tight_layout()
plt.show()