import cv2
import numpy as np
from matplotlib import pyplot as plt

# Leer la imagen binaria
I = cv2.imread('hola_ruido.png', cv2.IMREAD_GRAYSCALE)

# Mostrar la imagen original
plt.figure(figsize=(12, 12))

plt.subplot(4, 3, 1)
plt.imshow(I, cmap='gray')
plt.title('Original')
plt.axis('off')

# Crear elementos estructurantes
# Aproximación de un diamante
def create_diamond(size):
    diamond = np.zeros((2 * size + 1, 2 * size + 1), dtype=np.uint8)
    for i in range(size + 1):
        diamond[size - i:size + i + 1, i] = 1
        diamond[size - i:size + i + 1, 2 * size - i] = 1
    return diamond

se1 = create_diamond(5)
se2 = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (5, 5))  # Disk of radius 2
se3 = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (5, 5))  # Disk of radius 2 with no shift
se4 = cv2.getStructuringElement(cv2.MORPH_CROSS, (13, 13))  # Approximation of an octagon
se5 = cv2.getStructuringElement(cv2.MORPH_RECT, (5, 1))  # Line (horizontal)
se6 = cv2.getStructuringElement(cv2.MORPH_RECT, (4, 5))  # Rectangle of size 4x5
se7 = cv2.getStructuringElement(cv2.MORPH_RECT, (4, 4))  # Square of size 4x4
# se8 = not applicable in 2D image processing
# se9 = not applicable in 2D image processing
# se10 = not applicable in 2D image processing

# Dilatación
I_dilated1 = cv2.dilate(I, se1)
I_dilated2 = cv2.dilate(I, se2)
I_dilated3 = cv2.dilate(I, se3)
I_dilated4 = cv2.dilate(I, se4)
I_dilated5 = cv2.dilate(I, se5)
I_dilated6 = cv2.dilate(I, se6)
I_dilated7 = cv2.dilate(I, se7)
# I_dilated8 = not applicable in OpenCV
# I_dilated9 = not applicable in OpenCV
# I_dilated10 = not applicable in OpenCV

# Mostrar las imágenes
plt.subplot(4, 3, 3)
plt.imshow(I_dilated1, cmap='gray')
plt.title('diamond')
plt.axis('off')

plt.subplot(4, 3, 4)
plt.imshow(I_dilated2, cmap='gray')
plt.title('disk1')
plt.axis('off')

plt.subplot(4, 3, 5)
plt.imshow(I_dilated3, cmap='gray')
plt.title('disk2')
plt.axis('off')

plt.subplot(4, 3, 6)
plt.imshow(I_dilated4, cmap='gray')
plt.title('octagon')
plt.axis('off')

plt.subplot(4, 3, 7)
plt.imshow(I_dilated5, cmap='gray')
plt.title('line')
plt.axis('off')

plt.subplot(4, 3, 8)
plt.imshow(I_dilated6, cmap='gray')
plt.title('rectangle')
plt.axis('off')

plt.subplot(4, 3, 9)
plt.imshow(I_dilated7, cmap='gray')
plt.title('square')
plt.axis('off')

plt.tight_layout()
plt.show()