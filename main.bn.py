import numpy as np
import matplotlib.pyplot as plt

def create_chessboard(size=8):
    # Creamos una matriz numpy para representar el tablero de ajedrez
    board = np.zeros((size, size))

    # Rellenamos el tablero con 1 y 0 alternados para representar las casillas blancas y negras
    for i in range(size):
        for j in range(size):
            if (i + j) % 2 == 0:
                board[i][j] = 1

    return board

def plot_chessboard(board):
    plt.imshow(board, cmap='binary', origin='upper')
    plt.show()

if __name__ == "__main__":
    size = 4
    board = create_chessboard(size)
    plot_chessboard(board)