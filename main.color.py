import numpy as np
import matplotlib.pyplot as plt

def create_colored_chessboard(size=8, light_color='YlOrRd', dark_color='YlOrRd'):
    board = np.zeros((size, size, 3), dtype=np.uint8)

    light_rgb = [255,255,255]#np.array(plt.get_cmap(light_color)(0)[:3] * 255, dtype=np.uint8)
    dark_rgb = [207,52,52]#np.array(plt.get_cmap(dark_color)(0)[:3] * 255, dtype=np.uint8)
    print("board",board)
    print("light_rgb",light_rgb)
    print("dark_rgb",dark_rgb)
    for i in range(size):
        print("i",i)
        for j in range(size):
            print("j",j)
            if (i + j) % 2 == 0:
                board[i][j] = light_rgb#light_rgb  # Color claro
            else:
                board[i][j] = dark_rgb#dark_rgb  # Color oscuro

    return board

def plot_colored_chessboard(board):
    plt.imshow(board)
    plt.axis('off')  # Oculta los ejes
    plt.show()

if __name__ == "__main__":
    size = 8
    board = create_colored_chessboard(size)
    plot_colored_chessboard(board)